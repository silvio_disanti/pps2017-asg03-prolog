%size(List,Size): Size will contain the number of elements in List
size([],0).
size([_|T],M):-size(T,N),M is N+1.

%size(List,Size): Size will contain the number of elements in List,
%written using notation zero,s(zero),s(s(zero))...
size_with_s([],zero).
size_with_s([_|T],M):-size_with_s(T,N), M = s(N).

%sum(List,Sum)
sum([],0).
sum([H|T],M):-sum(T,N), M is N+H.

%average(List,Average): it uses average(List,Count,Sum,Average)
average(List,A):-average(List,0,0,A).
average([],C,S,A):-A is S/C.
average([X|Xs],C,S,A):-
				C2 is C+1,
				S2 is S+X,
				average(Xs,C2,S2,A).

%max(List,Max): Max is the biggest element in List
%It uses max(List,TempMax,Max) where TempMax is the maximum found so far
max([X|Xs],M):-max(Xs,X,M).
max([X|Xs],T,M):-X>T, max(Xs,X,M).
max([X|Xs],T,M):-X=<T, max(Xs,T,M).
max([],T,T). %when all the elements in the list are checked, set Max to the last TempMax