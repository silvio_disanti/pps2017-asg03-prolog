%search(Elem,List) an element in a list (from Ex. 1.1)
search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

%same(List1,List2)
same([],[]).
same([X|Xs],[Y|Ys]):-same(Xs,Ys).

%all_bigger(List1,List2): all elements in List1 are bigger than those in List2, 1 by 1
all_bigger([H1],[H2]):-H1>H2.
all_bigger([H1|T1],[H2|T2]):-H1>H2, all_bigger(T1,T2).

%sublist(List1,List2): List1 should be a subset of List2
sublist([X],List2):-search(X,List2).
sublist([X|Xs],List2):-search(X,List2), sublist(Xs,List2).