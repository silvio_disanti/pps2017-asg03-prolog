%inv(List,List)
inv(List,Inv):-inv(List,[],Inv).
inv([],Inv,Inv).
inv([H|T],Acc,Inv):-inv(T,[H|Acc],Inv).

%double(List,List)
double(List,List2):-append(List,List,List2).

%times(List,N,List)
times([_|_],0,[]).
times(L,1,L).
times(L,N,L2):-
				N>1,
				N2 is N-1,
				times(L,N2,L3),
				append(L,L3,L2).

%proj(List,List)
proj(X,Y):-proj(X,[],Y).
proj([],Y,Y).
proj([[H|_]|T],N,Y):-
				append(N,[H],N2),
				proj(T,N2,Y).