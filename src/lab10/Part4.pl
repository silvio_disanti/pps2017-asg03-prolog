%seq(N,List)
seq(0,[]).
seq(N,[0|T]):-N>0, N2 is N-1, seq(N2,T).

%seqR(N,List)
seqR(0,[]).
seqR(N,[N|T]):-N>0, N2 is N-1, seqR(N2,T).

%last(List1,Elem,List2): utility predicate
last([],N,[N]).
last([X|Xs],N,[X|Ys]):-last(Xs,N,Ys).

%seqR2(N,List)
seqR2(0,[0]).
seqR2(N,L):-
				N>0,
				N2 is N-1,
				seqR2(N2,L2),
				last(L2,N,L). 

