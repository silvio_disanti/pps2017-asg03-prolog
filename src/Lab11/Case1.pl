%dropAny(?Elem,?List,?OutList): drops any occurence of Elem
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

%dropFirst(?Elem,?List,?OutList): drops only the first occurence of Elem
dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

%dropLast(?Elem,?List,?OutList): drops only the last occurence of Elem
dropLast(X,[X|T],T):-not(dropLast(X,T,_)),!.
dropLast(X,[H|Xs],[H|L]):-dropLast(X,Xs,L).

%dropAll(?Elem,?List,?OutList): drops all the occurences of Elem returning a single list as result
dropAll(_,[],[]).
dropAll(X,[X|T],R):-!, dropAll(X,T,R).
dropAll(X,[H|Xs],[H|L]):-dropAll(X,Xs,L).