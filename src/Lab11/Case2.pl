%fromList(+List,-Graph): obtains a graph from a list
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):-fromList([H2|T],L).

%fromCircList(+List,-Graph):obtains a graph from a circular list
fromCircList([H|T],G):-fromCircList([H|T],H,G).
fromCircList([X],N,[e(X,N)]):-!.
fromCircList([H1,H2|T],N,[e(H1,H2)|L]):-fromCircList([H2|T],N,L).

%dropAll(?Elem,?List,?OutList): drops all the occurences of Elem returning a single list as result (Ex. 1.1)
dropAll([],_,[]).
dropAll([X|T],X,R):-!, dropAll(T,X,R).
dropAll([H|Xs],X,[H|L]):-dropAll(Xs,X,L).

%dropNode(+Graph,+Node,-OutGraph): drop all edges starting and leaving from a Node
dropNode(G,N,G):-dropAll(G,e(N,_),G),
								 dropAll(G,e(_,N),G),
								 !.
				
dropNode(G,N,O):-dropAll(G,e(N,_),G2),
								 dropAll(G2,e(_,N),G3),
								 dropNode(G3,N,O).

%reaching(+Graph,+Node,-List): all the nodes that can be reached in 1 step from Node
reaching(G,N,L):-findall(X,member(e(N,X),G),L1),
								 findall(Y,member(e(Y,N),G),L2),
								 append(L1,L2,L).

%anypath(+Graph,+Node1,+Node2,-ListPath): a path from Node1 to Node2, if there are many path, they are showed 1-by-1
anypath([e(N1,N2)|_],N1,N2,[e(N1,N2)]).

anypath([e(N1,N2)|T],N1,N3,[e(N1,N2)|L]):-
				anypath(T,N2,N3,L).

anypath([e(_,_)|T],N1,N2,L):-
				anypath(T,N1,N2,L).

%allreaching(+Graph,+Node,-List): all the nodes that can be reached from Node.
%Suppose the graph is NOT circular
allreaching(G,N,L):-
				findall(X,anypath(G,N,X,_),L1),
				setof(Y,member(Y,L1),L).