%A really basic Tic Tac Toe implementation in Prolog
%The table is defined as a list of three elements, each one representing a row of the table
%%%%%%%%%%%%%%%
%    Table    %
%   [1,2,3],  %
%   [4,5,6],  %
%   [7,8,9]   %
%%%%%%%%%%%%%%%

%Definition of the winning conditions
colWin(Table,Player):-Table =
				[Player,_,_,Player,_,_,Player,_,_].
colWin(Table,Player):-Table =
				[_,Player,_,_,Player,_,_,Player,_].
colWin(Table,Player):-Table =
				[_,_,Player,_,_,Player,_,_,Player].

rowWin(Table,Player):-Table =
				[Player,Player,Player,_,_,_,_,_,_].
rowWin(Table,Player):-Table =
				[_,_,_,Player,Player,Player,_,_,_].
rowWin(Table,Player):-Table =
				[_,_,_,_,_,_,Player,Player,Player].

diagWin(Table,Player):-Table =
				[Player,_,_,_,Player,_,_,_,Player].
diagWin(Table,Player):-Table =
				[_,_,Player,_,Player,_,Player,_,_].

%win(+Table,+Player) checks if a player is in a winning condition
win(Table,Player):-colWin(Table,Player).
win(Table,Player):-rowWin(Table,Player).
win(Table,Player):-diagWin(Table,Player).

%even(+Table,+Player) checks if no one win
even(Table,Player):-not(member(z,Table)).

%turn(+Player1,+Player2)
turn(x,o).
turn(o,x).

%game(+Table,+Player)
game(Table,Player):-win(Table,Player),!,write([player,Player,wins]),nl,nl.
game(Table,Player):-even(Table,Player),!,write([draw,game]),nl,nl.
game(Table,Player1):-
				turn(Player1,Player2),
				%selectMove(AvailMoves,N,RemainMoves),
				move(Table,Player1,NewTable),
				!,
				display(NewTable),
				game(NewTable,Player2).

%move(+Table,+Player,-NewTable)
%represent all the moves allowed, starting from Table and the Player it returns the player's move
move([z,B,C,D,E,F,G,H,I],Player,[Player,B,C,D,E,F,G,H,I]).
move([A,z,C,D,E,F,G,H,I],Player,[A,Player,C,D,E,F,G,H,I]).
move([A,B,z,D,E,F,G,H,I],Player,[A,B,Player,D,E,F,G,H,I]).
move([A,B,C,z,E,F,G,H,I],Player,[A,B,C,Player,E,F,G,H,I]).
move([A,B,C,D,z,F,G,H,I],Player,[A,B,C,D,Player,F,G,H,I]).
move([A,B,C,D,E,z,G,H,I],Player,[A,B,C,D,E,Player,G,H,I]).
move([A,B,C,D,E,F,z,H,I],Player,[A,B,C,D,E,F,Player,H,I]).
move([A,B,C,D,E,F,G,z,I],Player,[A,B,C,D,E,F,G,Player,I]).
move([A,B,C,D,E,F,G,H,z],Player,[A,B,C,D,E,F,G,H,Player]).

%From Ex. 1.1
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

%Randomly select a move from the moves available,
%this should select a random move to better simulate a game
%Now the engine just seeks for the first available cell to make a move
selectMove(L,N,L1):-length(L,X),rand_int(X,Y),dropAny(Y,L,L1),!.

%display(+Table)
display([A,B,C,D,E,F,G,H,I]):-write([A,B,C]),nl,
                              write([D,E,F]),nl,
                              write([G,H,I]),nl,nl.

%Simulate a game
play:-game([z,z,z,z,z,z,z,z,z],x).